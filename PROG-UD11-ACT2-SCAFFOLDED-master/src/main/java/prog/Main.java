package prog;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import prog.Validator.Validator;
import prog.enterprise.DbEnterpriseRepository;
import prog.enterprise.EnterpriseFormView;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Hello World");
        Scene scene = new Scene(new EnterpriseFormView(new Validator(), new DbEnterpriseRepository()), 400, 400);
        scene.getStylesheets().add(Main.class.getResource("../main.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();

    }

}
