package prog.enterprise;

import prog.exception.EnterpriseNotFoundException;

public interface EnterpriseRepository {

     boolean save(Enterprise enterprise);

     Enterprise findById(int enterpriseId);

     Enterprise getById(int enterpriseId) throws EnterpriseNotFoundException;

     Enterprise findByNif(String nif);

}
