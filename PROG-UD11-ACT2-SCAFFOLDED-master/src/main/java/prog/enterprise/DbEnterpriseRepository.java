package prog.enterprise;

import prog.repository.MySqlConnection;
import prog.exception.EnterpriseNotFoundException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbEnterpriseRepository implements EnterpriseRepository {

    private MySqlConnection mySqlConnection;

    public DbEnterpriseRepository(){

        this.mySqlConnection = new MySqlConnection("crm_db", "root", "peripoll9");

    }

    @Override
    public boolean save(Enterprise enterprise) {

        if (enterprise.getId() != null) {
            return this.update(enterprise);
        }

        return this.add(enterprise);

    }

    @Override
    public Enterprise findById(int enterpriseId) {

        String sql = "SELECT * FROM Enterprise WHERE id = "+enterpriseId;
        Connection connection = this.mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            if (rs.next()) {
                return new Enterprise(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("address"),
                        rs.getString("city"),
                        rs.getString("province"),
                        rs.getString("country"),
                        rs.getString("locale"),
                        rs.getString("nif"),
                        rs.getTimestamp("createdOn").toLocalDateTime(),
                        (rs.getInt("status") == 1)
                );
            }

            return null;

        } catch (SQLException e) {

            e.printStackTrace();
            return null;

        }

    }

    @Override
    public Enterprise getById(int enterpriseId) throws EnterpriseNotFoundException {

        Enterprise enterprise = findById(enterpriseId);

        if (enterprise != null) {
            return enterprise;
        }

        throw new EnterpriseNotFoundException();
    }

    @Override
    public Enterprise findByNif(String nif){

        Connection connection = this.mySqlConnection.getConnection();
        String sql = "SELECT * FROM Enterprise WHERE nif = '"+nif+"'";

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                String city = resultSet.getString("city");
                String province = resultSet.getString("province");
                String country = resultSet.getString("country");
                String locale = resultSet.getString("locale");
                String nif2 = resultSet.getString("nif");

                return new Enterprise(name, address, city, province, country, locale, nif2);
            }
            return null;

        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    private boolean update(Enterprise enterprise) {

        String sql = "UPDATE Enterprise SET ";
        sql+=   "name = '"+enterprise.getName()+"'," +
                "address = '"+enterprise.getAddress()+"'," +
                "city = '"+enterprise.getCity()+"'," +
                "province = '"+enterprise.getProvince()+"'," +
                "country = '"+enterprise.getCountry()+"'," +
                "locale = '"+enterprise.getLocale()+"'," +
                "nif = '"+enterprise.getNif()+"'," +
                "status = "+((enterprise.isActive()) ? 1 : 0) +
                " WHERE id = "+enterprise.getId();

        Connection connection = this.mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            return true;

        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        }

    }

    private boolean add(Enterprise enterprise) {

        String sql = "INSERT INTO Enterprise (name,address,city,province,country,locale,createdOn,nif,status) ";
        sql+="VALUES (" +
                "'"+enterprise.getName()+"'," +
                "'"+enterprise.getAddress()+"'," +
                "'"+enterprise.getCity()+"'," +
                "'"+enterprise.getProvince()+"'," +
                "'"+enterprise.getCountry()+"'," +
                "'"+enterprise.getLocale()+"'," +
                "'"+enterprise.getCreatedOnISOString()+"'," +
                "'"+enterprise.getNif()+"'," +
                +((enterprise.isActive()) ? 1 : 0) +
                " )";

        Connection connection = this.mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                int idEnterprise = generatedKeys.getInt(1);
                enterprise.setId(idEnterprise);
                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return false;
    }

}
